package luhn_algorithm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgoritmTest {

    LuhnAlgoritm luhn = new LuhnAlgoritm();

    @Test
    void validCardNumber() {
        assertEquals(0, (luhn.luhnsAlgorithm("123456789123456789")));
        assertEquals(0, (luhn.luhnsAlgorithm("23457896986080860")));
        assertEquals(0, (luhn.luhnsAlgorithm("4242424242424242")));
    }

    @Test
    void invalidCardNumber(){
        assertEquals(7, (luhn.luhnsAlgorithm("9853531246783567")));
        assertEquals(7, (luhn.luhnsAlgorithm("8205719503769372")));
    }

    @Test
    void checkCalculatedCheckDigit() {
        luhn.luhnsAlgorithm("4242424242424242");
        assertEquals(2,luhn.getCalculatedCheckDigit());
    }

    @Test
    void checkProvidedCheckDigit(){
        luhn.luhnsAlgorithm("4242424242424242");
        assertEquals(2, luhn.getProvidedCheckDigit());
    }

    @Test
    void invalidChecksum(){
        luhn.luhnsAlgorithm("4242424242424242");
        assertTrue(luhn.validChecksum());
    }

    @Test
    void validChecksum(){
        luhn.luhnsAlgorithm("42424242423424242");
        assertFalse(luhn.validChecksum());
    }

    @Test
    void checkLength(){
        luhn.luhnsAlgorithm("4242424242424242");
        assertEquals(16, luhn.getCardNumberLength());
    }

    @Test
    void validCreditCard(){
        luhn.luhnsAlgorithm("4242424242424242");
        assertTrue(luhn.validCreditCard());
    }

    @Test
    void invalidCreditCard(){
        luhn.luhnsAlgorithm("23457896986080860");
        assertFalse(luhn.validCreditCard());
    }
}