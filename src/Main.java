import luhn_algorithm.LuhnAlgoritm;

import java.util.Scanner;

public class Main {

    public static LuhnAlgoritm luhn = new LuhnAlgoritm();

    public static void main(String[] args) {

        commandOption();

    }

    public static void commandOption(){
        Scanner sc = new Scanner(System.in);
        System.out.println("What number would you like to check?");
        String input = "";
        try {
            while (input.equals("")) {
                if (sc.hasNextLine()) {
                    input = sc.nextLine();
                }
                try {
                    luhn.luhnsAlgorithm(input);
                    printOutput(input);
                } catch (Exception e){
                    System.out.println(e);
                    System.out.println("Invalid input! Only numbers is valid input!");
                    input = "";
                }
            }

        } catch (Exception e){
            System.out.println(e);
        } finally {
            sc.close();
        }
    }

    private static void printOutput(String input) {
        System.out.println("Input: " + luhn.getCardNumberWithoutCheck() + " " + luhn.getProvidedCheckDigit());
        System.out.println("Provided: " + luhn.getProvidedCheckDigit());
        System.out.println("Expected: " + luhn.getCalculatedCheckDigit());
        if (luhn.validChecksum()){
            System.out.println("\nChecksum: Valid");
        } else {
            System.out.println("\nChecksum: Not valid");
        }
        if (luhn.validCreditCard()) {
            System.out.println("Digits: " + luhn.getCardNumberLength() + " (credit card)");
        } else {
            System.out.println("Digits: " + luhn.getCardNumberLength());
        }
    }
}
