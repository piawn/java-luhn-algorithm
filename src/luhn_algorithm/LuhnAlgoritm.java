package luhn_algorithm;

public class LuhnAlgoritm {

    private String cardNumberWithoutCheck = "";
    private int cardNumberLength = 0;
    private int calculatedCheckDigit = 0;
    private int providedCheckDigit = 0;
    private boolean isValidCreditCard = false;

    /*
    * Luhn Algorithm
    * From the rigthmost digit, double every second digit.
    * If digit > 9, add separated digits from result to sum.
    * If sum modulo 10 equals 0, the number is valid.
    */
    public int luhnsAlgorithm(String cardNo){
        int numLength = cardNo.length();
        initVariables(cardNo);
        int sum = 0;
        boolean isEven = false;

        for (int i = numLength-1; i >= 0; i--){
            int current = Integer.parseInt(cardNo.substring(i, i+1));
            if (isEven) {
                current *= 2;
                if (current > 9) {
                    current = (current % 10) + 1;
                }
            }
            sum += current;
            isEven = !isEven;
        }

        calculateCheckDigit(sum);

        return sum%10;
    }

    private void initVariables(String cardNo) {
        this.cardNumberLength = cardNo.length();
        this.providedCheckDigit = Integer.parseInt(cardNo.substring(cardNumberLength-1));
        this.cardNumberWithoutCheck = cardNo.substring(0, this.cardNumberLength - 1);

        if (this.cardNumberLength == 16){
            this.isValidCreditCard = true;
        }
    }

    /*
    * To calculate the check digit, take the sum (excluding
    * the check digit), multiply with 9 and then use modulo 10.
    */
    private void calculateCheckDigit(int sum){
        int newSum = sum - this.providedCheckDigit;
        this.calculatedCheckDigit = (newSum*9)%10;
    }

    public int getCalculatedCheckDigit(){
        return this.calculatedCheckDigit;
    }

    public int getProvidedCheckDigit(){
        return this.providedCheckDigit;
    }

    public int getCardNumberLength(){
        return this.cardNumberLength;
    }

    public boolean validChecksum(){
        return this.providedCheckDigit == this.calculatedCheckDigit;
    }

    public String getCardNumberWithoutCheck(){
        return this.cardNumberWithoutCheck;
    }

    public boolean validCreditCard(){
        return this.isValidCreditCard;
    }
}
